# Tarrots Badger2040 Project

## How to install
You first need a badger2040 with micropython flashed to it and the badger default demo installed!  Drag and drop `main.py`, `faces.txt`, and `qr.bin` to the root of the badger2040.
You can generate a new qr.bin and replace the qr code in the display.

## info
After install your badger button config will be:
A: Return to mode 1 ( Badge mode )
B: Boop.
C: Display QR code.
Up: Make mood better
Down: Make mood worse
