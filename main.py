#import _launcher
import badger2040
from random import choice
from time import sleep
badger = badger2040.Badger2040()

#
# Edit theses ^~^
#
badge_name = "Tarrot"
badge_url = "https://tarrot2346.cardd.co"

#
# Careful editing below this :3
#
badger.font ("bitmap8")
frl = open("faces.txt", "r").read().split("\n")
faces = [[],[],[],[],[],[],[]]
qr = bytearray(int(128*128/8))
try:
    open("qr.bin", "b").readinto(qr)
except:
    pass
mood = 0

def ctext(text):
    cts = 8
    for i in range(80, 0, int(-1)):
        cts = badger.measure_text(text, scale=int(i/10))
        if cts < 296:
            badger.pen(15)
            badger.clear()
            badger.pen(0)
            badger.text(f"{text}", int((296/2) - (cts/2)), 30, scale=8)
            return 0

def face(text):
    cts = badger.measure_text(text, scale=8)
    badger.pen(15)
    badger.clear()
    badger.pen(0)
    badger.text(f"{text}", int((296/2) - (cts/2)), 30, scale=8)

def btext(text):
    badger.text(f"{text}", int((296/2) - (badger.measure_text(text, scale=2)/2)), 110, scale=2)

def fupdate():
        face(choice(faces[mood]))
        btext(badge_url)
        badger.update()

if __name__ == "__main__":
    update = True
    for i in frl:
        faces[int(i.split(',')[0])].append(i.split(',')[1])
    loops = 0
    btnlast = False
    mode = 0
    #mood = choice(range(len(faces) - 1))
    
    # Main Loop
    while True:
        if mode == 0:
            if update: # Update face
                fupdate()
                update = False
            if not btnlast:
                if badger.pressed(12): # A button
                    mode = 1
                    update = True
                    btnlast = True
                if badger.pressed(13): # B Button
                     updatet = False
                     face(">W<")
                     btext("* boop *")
                     badger.update()
                     btnlast = True
                if badger.pressed(14): # C button
                    loops = 0
                    badger.pen(15)
                    badger.clear()
                    badger.image(qr, w=128, h=128, x=int((296/2) - (128/2)), y=0)
                    badger.update()
                    btnlast = True
                if badger.pressed(15): # Up button
                    mood += 1
                    if mood == len(faces):
                        print("overflow")
                        mood = 0
                    fupdate()
                    btnlast = True
                if badger.pressed(11): # Down button
                    mood -= 1
                    if mood == -1:
                        mood = len(faces) - 1
                    fupdate()
                    btnlast = True
                if badger.pressed(23): # boot/usr button
                    fupdate()
                    print(f"mood: {mood}")
                    btnlast = True
            else:
                btnlast = False
                for i in [11,12,13,14,15,23]:
                    if badger.pressed(i):
                        btnlast = True
        if mode == 1:
            if update:
                ctext(badge_name)
                btext(badge_url)
                badger.update()
                update = False
            if not btnlast:
                if badger.pressed(12): # A button
                    mode = 0
                    update = True
                    btnlast = True
                if badger.pressed(13): # B Button
                    update = True
                    btnlast = True
                if badger.pressed(14): # C button
                    loops = 0
                    badger.pen(15)
                    badger.clear()
                    badger.image(qr, w=128, h=128, x=int((296/2) - (128/2)), y=0)
                    badger.update()
                    btnlast = True
                if badger.pressed(15): # Up button
                    btnlast = True
                if badger.pressed(11): # Down button
                    btnlast = True
                if badger.pressed(23): # boot/usr button
                    btnlast = True
            else:
                btnlast = False
                for i in [11,12,13,14,15,23]:
                    if badger.pressed(i):
                        btnlast = True
                    
        sleep(.005)
        badger.led(loops % 256)
        loops += 1
        if loops == 5121:
            update = True
            if choice(range(10)) == 10:
                mood = choice(range(len(faces) - 1))
            fupdate()
            loops = 0
